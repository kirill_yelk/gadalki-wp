<?php get_header();?>
<main class="newsPage pageMagic">
    <section class="headSecondBlock">
    </section>
    <div class="container zIndex">
        <ul class="breadcrumbs-one">
            <li><a href="/" rel="nofollow">Главная</a></li>
            <li><a href="/rejting-magov" rel="nofollow">Рейтинг магов</a></li>
            <li><?php the_title();?></li>
        </ul>
    </div>
    <section class="headMagBlock">
        <div class="container">
            <div class="wrappStyle">
                <div class="imgBlock">
                    <?php the_post_thumbnail();?>
                    <div class="voitBlock">
                    	<?php 
                        $post_ID = $post->ID;
                        $metablog = new stdClass;
                        foreach( get_post_meta( $post_ID ) as $k => $v )
                        $metablog->$k = $v[0];
                    	?>
                        <?php do_action('count_likes_html', $post_ID) ?>
                        <div class="cir">
                            <?php do_action('likes_html', $post_ID) ?>
                        </div>
                    </div>
                </div>
                <div class="infoBlock">
                    <div class="name">
                        <?php the_title();?>
                    </div>
                    <div class="type">
                        <div class="typeBlock">
                        	<?php 
                                $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
                                    if( is_array( $cur_terms ) ){
                                        foreach( $cur_terms as $cur_term ){
                                            echo  $cur_term->name;
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div class="uslugi">
                        <div class="first">
                            Услуги:
                        </div>
                        <?php 
                            $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_tags' );
                                if( is_array( $cur_terms ) ){
                                    foreach( $cur_terms as $cur_term ){
                                        echo '<div class="uslugiBlock">'. $cur_term->name.'</div>';
                                }
                            }
                        ?>
                    </div>
                    <div class="phone">
                        <a href="tel:<?php echo preg_replace('/\D+/', '', get_field('phone-number'));?>">
                            <?php the_field('phone-number');?>
                        </a>
                    </div>
                    <div class="url">
                        <a target="_blank" href="<?php the_field('web-link');?>">
                            <?php the_field('web-link');?>
                        </a>
                    </div>
                    <div class="mail">
                        <a href="mailto:<?php the_field('email');?>">
                            <?php the_field('email');?>
                        </a>
                    </div>
                    <div class="maps">
                        <?php the_field('location');?>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="contentMain">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="topMag">
                        <div class="head">
                            топ магов
                        </div>
                        <ul>
                            <?php
                                $top = 1;

                                $args = array(
                                    'post_type' => 'fortune_tellers',
                                    'post_status' => 'publish',
                                    'posts_per_page' => '5',
                                    'meta_key' => 'likes_data',
                                    'orderby' => 'meta_value_num'
                                );

                                $query = new WP_Query( $args );
                            ?>
                            <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                            <?php 
                                $post_ID = $post->ID;
                                $metablog = new stdClass;
                                foreach( get_post_meta( $post_ID ) as $k => $v )
                                $metablog->$k = $v[0];
                            ?>
                            <li>
                                <div class="name">
                                    <span><?php echo  $top++;?></span>
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </div>
                                <div class="static">
                                    <?php do_action('number_likes_html', $post_ID) ?>
                                    <div class="comit">
                                        <a href="<?php the_permalink();?>">
                                            <?php echo get_comments_number($post->ID);?>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php endwhile; else : ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                    <div class="blockAddMag">
                        <div class="name">Добавьте специалиста</div>
                        <p>Товарищи! реализация намеченных плановых заданий</p>
                        <button data-toggle="modal" data-target="#exampleModal">Добавить</button>
                    </div>
                    <div class="newReviews hiddeMob">
                        <div class="head">
                            Новые отзывы
                        </div>
                        <?php 
                        $i = 0;
                        foreach (get_comments() as $comment): ?>
                        <a href="<?php the_permalink();?>">
                            <div class="news">
                                <div class="iconAb"></div>
                                <div class="name">
                                    <?php echo $comment->comment_author; ?>
                                </div>
                                <div class="discrition">
                                    <?php echo $comment->comment_content; ?>
                                </div>
                                <time>
                                    <?php comment_date('d.m.Y в H:i'); ?>
                                </time>
                            </div>
                        </a>
                        <?php $i++; ?>
                        <?php if($i == 1) break; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="newArticleSidbar hiddeMob">
                        <div class="head">
                            новые статьи
                        </div>
                        <ul>
                        	<?php
			                    $args = array(
			                        'post_type' => 'post',
			                        'post_status' => 'publish',
			                        'posts_per_page' => '4',
			                        'order' => 'DESC'
			                    );

			                    $query = new WP_Query( $args );
			                ?>
			                <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                            <li>
                                <div class="title">
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </div>
                                <div class="discript js-discrition">
                                    <?php echo get_the_excerpt(); ?>
                                </div>
                                <a href="<?php the_permalink();?>" class="btnClick">
                                    Читать
                                </a>
                            </li>
                            <?php endwhile; else : ?>
               				<?php endif; ?>
                			<?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <section class="textBox">
                        <div class="text">
                            <div class="titleBlock">
                                <div class="text">
                                    <?php the_title();?> Обзор
                                </div>
                                <div class="line">
                                </div>
                            </div>
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <?php the_content();?>
                            <?php endwhile; ?>
							<?php endif; ?>
                        </div>
                        <div class="itog">
                            <span>Итог:</span>
                            <?php the_field('final_desc');?>
                        </div>
                        <div class="voitBlock">
                            <?php 
                            $post_ID = $post->ID;
                            $metablog = new stdClass;
                            foreach( get_post_meta( $post_ID ) as $k => $v )
                            $metablog->$k = $v[0];
                            ?>
                            <?php do_action('number_likes_html', $post_ID) ?>
                            <div class="cir">
                                <?php do_action('likes_html', $post_ID) ?>
                            </div>
                        </div>
                        <h2 class="h2Block">Похожие специалисты:</h2>
                        <div class="vertical-center-4 slider listMagic listMagicSlider">
                        	<?php
                        	$top = 1;
		                    $tags = wp_get_post_terms( get_queried_object_id(), 'fortune_tellers_categories', ['fields' => 'ids'] );
		                    $args = [
		                        'post__not_in'        => array( get_queried_object_id() ),
		                        'posts_per_page'      => 3,
		                        'orderby'             => 'rand',
		                        'tax_query' => [
		                            [
		                                'taxonomy' => 'fortune_tellers_categories',
		                                'terms'    => $tags
		                            ]
		                        ]
		                    ];
		                    $my_query = new wp_query( $args );
		                    if( $my_query->have_posts() ) {
		                            while( $my_query->have_posts() ) {
		                                $my_query->the_post(); ?>
		                                    <div class=" col-lg-4 col-md-6 col-6">
				                                <div class="magic">
				                                    <?php the_post_thumbnail();?>
				                                    <div class="absolutTop">
				                                        <?php echo $top++;?>
				                                    </div>
				                                    <div class="boxInfo">
				                                        <div class="type">
				                                            <?php 
						                                    $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
						                                    if( is_array( $cur_terms ) ){
						                                        foreach( $cur_terms as $cur_term ){
						                                            echo  $cur_term->name;
						                                        }
						                                    }
						                                    ?>
				                                        </div>
				                                        <div class="name">
				                                            <?php the_title();?>
				                                        </div>
				                                        <div class="ratingBlock">
				                                            <?php 
                                                                $likes_data = get_post_meta( $id, 'likes_data', true );
                                                                $likes_data = $likes_data ? $likes_data : "+0";
                                                                $likes_data = ($likes_data > 0) ? '+'.$likes_data : $likes_data;
                                                            ?>
                                                            <div class="rating">Рейтинг: <b><span><?= $likes_data ?></span></div></b>
				                                            <div class="comit">
				                                                <a href="<?php the_permalink();?>">
						                                            <?php echo get_comments_number($post->ID);?>
						                                        </a>
				                                            </div>
				                                        </div>
				                                        <div class="discrition js-discrition">
				                                            <?php echo get_the_excerpt(); ?>
				                                        </div>
				                                        <div class="skill">
				                                            <?php 
						                                    $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_tags' );
						                                    if( is_array( $cur_terms ) ){
						                                        foreach( $cur_terms as $cur_term ){
						                                            echo  $cur_term->name .', ';
						                                        }
						                                    }
						                                    ?>
				                                        </div>
				                                        <button onclick="return location.href = '<?php the_permalink();?>'">Подробнее</button>
				                                    </div>
				                                </div>
				                            </div>          
		                    <?php }
		                        wp_reset_postdata();
		                    }
		                    ?>
                        </div>

                        <div class="listCommit" id="listCommit">
                            <div class="container">
                                <div class="addCommit">
                                    <h2>Отзывы</h2>
                                    <?php if (is_user_logged_in()) { ?>
                                        <p class="pp"> Оставьте свой отзыв о работе специалиста:</p>
                                        <form id="addcomment" action="" class="formCommit">
                                            <input id="comment_post_ID" type="hidden" name="comment_post_ID" value="<?= $post_ID ?>">
                                            <textarea cols="45" rows="12" name="comment"></textarea>
                                            <div class="bottom">
                                                <div>
                                                    <p class="pol">
                                                        <input type="radio" id="test1" name="radio-group" value="1">
                                                        <label for="test1">Положительный</label>
                                                    </p>
                                                    <p class="otr">
                                                        <input type="radio" id="test2" name="radio-group" value="-1">
                                                        <label for="test2">Отрицательный</label>
                                                    </p>
                                    
                                                </div>
                                                <button>Оставить отзыв</button>
                                            </div>
                                        </form>

                                        <?php } else { ?>
                                            <?php echo get_ulogin_panel(); ?>
                                        <?php } ?>
                                </div>
                                <div class="row">
                                    <div class="boxCommit">
                                        <?php
                                        // Получаем комментарии поста с ID XXX из базы данных
                                        $comments = get_comments(array(
                                            'post_id' => $post_ID,
                                            'status' => 'approve' // комментарии прошедшие модерацию
                                        ));

                                        // Формируем вывод списка полученных комментариев
                                        echo wp_list_comments(array(
                                            'per_page' => -1, // Пагинация комментариев - по 10 на страницу
                                            'callback' => 'mytheme_comment',
                                            'style' => 'div',
                                            'reverse_top_level' => false // Показываем последние комментарии в начале
                                        ), $comments);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>
<?php get_footer();?>