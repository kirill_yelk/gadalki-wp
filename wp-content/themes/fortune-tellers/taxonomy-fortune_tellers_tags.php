<?php get_header(); ?>
<main class="newsListPage">
	<section class="headSecondBlock">
    </section>
	<div class="container zIndex">
        <ul id="breadcrumbs" class="breadcrumbs-one">
            <li><a href="/" rel="nofollow">Главная</a></li>
            <li><?php single_term_title(); ?></li>
        </ul>
    </div>

    <section class="blockForm">
        <?php get_search_form();?>
    </section>
    <section class="newArticle">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    Маги
                </div>
                <div class="line">
                </div>
            </div>
        </div>
    </section>
    <section class="contentMain">
	    <div class="container">
		    <div class="row">
		    <div class="col-lg-3 col-md-4">
                    <div class="topMag">
                        <div class="head">
                            топ магов
                        </div>
                        <ul>
                            <?php
                                $top = 1;

                                $args = array(
                                    'post_type' => 'fortune_tellers',
                                    'post_status' => 'publish',
                                    'posts_per_page' => '5',
                                    'meta_key' => 'likes_data',
                                    'orderby' => 'meta_value_num'
                                );

                                $query = new WP_Query( $args );
                            ?>
                            <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                            <?php 
                                $post_ID = $post->ID;
                                $metablog = new stdClass;
                                foreach( get_post_meta( $post_ID ) as $k => $v )
                                $metablog->$k = $v[0];
                            ?>
                            <li>
                                <div class="name">
                                    <span><?php echo  $top++;?></span>
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </div>
                                <div class="static">
                                    <?php do_action('count_likes_html', $post_ID) ?>
                                    <div class="comit">
                                        <a href="<?php the_permalink();?>">
                                            <?php echo get_comments_number($post->ID);?>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php endwhile; else : ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
	            <div class="col-lg-9 col-md-8">  
		            <div class="textBox">
			            <div class="listMagic">
				            <div class="row">  
							<?php 
								$top = 1;
								$category = get_queried_object();
								$args = array(
									'post_type'         => 'fortune_tellers',
									'posts_per_page'    =>  -1,
                                    'meta_key' => 'likes_data',
                                    'orderby' => 'meta_value_num',
									'tax_query' => array(
									array(
										'taxonomy' => 'fortune_tellers_tags',
										'field'    => 'term_id',
										'terms'    => $category->term_id
										)
									)
								); 
								// assign arguments to new WP_Query
								$query = new WP_Query( $args );?>
								<?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
                                    <?php 
                                        $post_ID = $post->ID;
                                        $metablog = new stdClass;
                                        foreach( get_post_meta( $post_ID ) as $k => $v )
                                        $metablog->$k = $v[0];
                                    ?>
									<div class="col-lg-4 col-md-6 col-6">
										<div class="magic">
											<?php the_post_thumbnail();?>
                                        <div class="absolutTop">
                                            Топ <?php echo  $top++;?>
                                        </div>
                                        <div class="boxInfo">
                                            <div class="type">
                                                <?php 
                                                $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
                                                if( is_array( $cur_terms ) ){
                                                    foreach( $cur_terms as $cur_term ){
                                                        echo  $cur_term->name;
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="name">
                                                <?php the_title();?>
                                            </div>
                                            <div class="ratingBlock">
                                                <?php do_action('count_likes_html', $post_ID) ?>
                                                <div class="comit">
                                                    <a href="<?php the_permalink();?>">
                                                        <?php echo get_comments_number($post->ID);?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="discrition js-discrition">
                                                <?php echo get_the_excerpt(); ?>
                                            </div>
                                            <div class="skill">
                                                <?php 
                                                $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_tags' );
                                                if( is_array( $cur_terms ) ){
                                                    foreach( $cur_terms as $cur_term ){
                                                        echo  $cur_term->name .', ';
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <button onclick="return location.href = '<?php the_permalink();?>'">Подробнее</button>
                                        </div>
										</div>
									</div>
								<?php endwhile;?>
								<?php endif; ?>
								<?php wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer();?>