<?php


/**
 * ==================== Blog Page ======================
 * 2/13/2016
 */
add_action( 'wp_ajax_nopriv_ajx20161813101849', 'ajx20161813101849' );
add_action( 'wp_ajax_ajx20161813101849', 'ajx20161813101849' );
function ajx20161813101849()
{
	global $post;

    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1; // следующая страница
    $args['post_status'] = 'publish';
 
    // обычно лучше использовать WP_Query, но не здесь
    query_posts( $args );
    // если посты есть
    if( have_posts() ) :
 
        // запускаем цикл
        while( have_posts() ): the_post();

            $post_ID = get_the_ID();
            
            include( locate_template( 'templates/blog_layout/split_layout.php' ) );
 
        endwhile;
 
    endif;
    die();
}


/**
 * ==================== Likes Process ======================
 *
 * 2/16/2016
 */
add_action( 'wp_ajax_nopriv_ajx20161216111200', 'ajx20161216111200' );
add_action( 'wp_ajax_ajx20161216111200', 'ajx20161216111200' );
function ajx20161216111200()
{
	$user_hash = aa_get_current_user_hash();
	$post_id = $_POST[ 'post_id' ];
	$rating = (int) $_POST[ 'rating' ];
	$likes_arr = [];

	$likes_data = get_post_meta( $post_id, 'likes_data', true );
	$likes_data = $likes_data ? $likes_data : 0;

	if($rating > 0){
		$likes_data += 1;
		$data_result = 'like';
	} else {
		$likes_data -= 1;
		$data_result = 'unlike';
	}

	// ================== First Like ==================
	if ( post_like( $post_id ) === 0 ) {
		$likes_arr[] = $user_hash;
		update_post_meta( $post_id, 'likes_container', serialize( $likes_arr ) );
		update_post_meta( $post_id, 'likes_data', $likes_data );

		echo $data_result;
	} else {

		$likes_arr = unserialize( get_post_meta( $post_id, 'likes_container', true ) );

		$position  = array_search( $user_hash, $likes_arr );
		// ================== Unlike ==================
		if ( $position !== false ) {
			echo 'Вы уже проголосовали';
		} else {
			// ================== Add Like ==================
			$likes_arr[] = $user_hash;
			update_post_meta( $post_id, 'likes_container', serialize( $likes_arr ) );
			update_post_meta( $post_id, 'likes_data', $likes_data );

			echo $data_result;
		}
	}

	die;
}


/*****---------------------Добавление ajax коммента--------------------------****/

add_action('wp_ajax_ajaxcomments', 'true_add_ajax_comment'); // wp_ajax_{значение параметра action}
add_action('wp_ajax_nopriv_ajaxcomments', 'true_add_ajax_comment'); // wp_ajax_nopriv_{значение параметра action}
function true_add_ajax_comment(){
    global $wpdb;
    $comment_post_ID = isset($_POST['comment_post_ID']) ? (int) $_POST['comment_post_ID'] : 0;
 
    $post = get_post($comment_post_ID);
 
    if ( empty($post->comment_status) ) {
        do_action('comment_id_not_found', $comment_post_ID);
        exit;
    }
 
    $status = get_post_status($post);
 
    $status_obj = get_post_status_object($status);
 
    /*
     * различные проверки комментария
     */
    if ( !comments_open($comment_post_ID) ) {
        do_action('comment_closed', $comment_post_ID);
        wp_die( __('Sorry, comments are closed for this item.') );
    } elseif ( 'trash' == $status ) {
        do_action('comment_on_trash', $comment_post_ID);
        exit;
    } elseif ( !$status_obj->public && !$status_obj->private ) {
        do_action('comment_on_draft', $comment_post_ID);
        exit;
    } elseif ( post_password_required($comment_post_ID) ) {
        do_action('comment_on_password_protected', $comment_post_ID);
        exit;
    } else {
        do_action('pre_comment_on_post', $comment_post_ID);
    }
 
    $comment_author       = ( isset($_POST['author']) )  ? trim(strip_tags($_POST['author'])) : null;
    $comment_author_email = ( isset($_POST['email']) )   ? trim($_POST['email']) : null;
    $comment_author_url   = ( isset($_POST['url']) )     ? trim($_POST['url']) : null;
    $comment_content      = ( isset($_POST['comment']) ) ? trim($_POST['comment']) : null;
    $comment_approved     = 1;
 
    /* 
     * проверяем, залогинен ли пользователь
     */
    $user = wp_get_current_user();
    if ( $user->exists() ) {

        if ( empty( $user->user_firstname ) && empty($user->user_lastname) ){

            $comment_author   = $user->display_name;
        } else {
   
            $comment_author   = $user->user_firstname." ".$user->user_lastname;
        }

        $comment_author_email = $wpdb->escape($user->user_email);
        $comment_author_url   = $wpdb->escape($user->user_url);
        $user_ID = get_current_user_id();
        if ( current_user_can('unfiltered_html') ) {
            if ( wp_create_nonce('unfiltered-html-comment_' . $comment_post_ID) != $_POST['_wp_unfiltered_html_comment'] ) {
                kses_remove_filters(); // start with a clean slate
                kses_init_filters(); // set up the filters
            }
        }
    } else {
        if ( get_option('comment_registration') || 'private' == $status )
            wp_die( 'You must register or login to leave comments!' );
    }
 
    $comment_type = '';
 
    if ( '' == trim($comment_content) ||  '<p><br></p>' == $comment_content )
        wp_die( 'You forgot to comment!' );
 
    /* 
     * добавляем новый коммент и сразу же обращаемся к нему
     */
    $comment_parent = isset($_POST['comment_parent']) ? absint($_POST['comment_parent']) : 0;
    $commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type', 'comment_parent', 'user_ID', 'comment_approved');
    $comment_id = wp_new_comment( $commentdata );

    $comment_add = add_comment_meta( $comment_id, 'radio-group', $_POST['radio-group'] );

    $comment = get_comment($comment_id);
 
    /*
     * выставляем кукисы
     */
    do_action('set_comment_cookies', $comment, $user);
 
    /*
     * вложенность комментариев
     */
    $comment_depth = 1;
 
    $GLOBALS['comment'] = $comment;
    $GLOBALS['comment_depth'] = $comment_depth;

    /*
     * ниже идет шаблон нового комментария, вы можете настроить его для себя,
     * а можете воспользоваться функцией(которая скорее всего уже есть в теме) для его вывода
     */
    if(isset($_POST['comment_parent']) ) : ?>

        <div id="commit-<?= $comment->comment_ID ?>" class="commit blockAnswerCommit">
            <div class="wrappCommit">
                <?php echo get_avatar($comment, $size='60' , '', '', array('') ); ?>
                <div class="commitInfo">
                    <div class="commitInfo-head">
                        <div class="left">
                            <p class="name"><?php echo get_comment_author(); ?>
                                <span>Ответ для <b><?php echo get_comment_author($comment->comment_parent); ?></b></span></p>
                            <time><?php echo get_comment_date('d.m.Y') ?></time>
                        </div>
                    </div>

    <?php else : ?>

        <div id="commit-<?= $comment->comment_ID ?>" class="commit">
            <div class="wrappCommit">
                <?php 
                    $rate = get_comment_meta( $comment_id, 'radio-group', true);
                    if($rate == 1){
                        echo '<div class="classPlus">+</div>';
                    }else{
                        echo '<div class="classMinus">-</div>';
                    }
                ?>
                <?php echo get_avatar($comment, $size='60' , '', '', array('') ); ?>
                <div class="commitInfo">
                    <div class="commitInfo-head">
                        <div class="left">
                            <p class="name"><?php echo get_comment_author(); ?></p>
                            <time><?php echo get_comment_date('d.m.Y') ?></time>
                        </div>
                    </div>

    <?php endif; ?>

                    <div class="commitText">
                        <p><?php echo get_comment_text() ?></p>
                    </div>
                    <div class="ridth">

                        <?php if(get_current_user_id() != $comment->user_id) : ?>

                            <button  class="js-OpenAnswer" data-id="<?= $comment->comment_ID ?>">Ответить</button>

                        <?php endif; ?>

                    </div>

                </div>
            </div>
            <div class="readAnswer"></div>
        </div>

<?php die();
}
