<footer>
    <div class="blockFooter">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a title="logo" href="/" class="styleLogo">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo2.png" alt="logo">
                    <div class="logoText">
                        <p class="logoText-P">ExtraTop</p>
                        <span>Честный рейтинг магов</span>
                    </div>
                </a>
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarsExample06"
                        aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between navMobil navMobil2 js-WidthNav" id="navbarsExample06">
                     <?php wp_nav_menu( 
                        array(
                            'menu_name' => 'Primary',
                            'menu_class' => 'navbar-nav'
                    ) ); ?>
                    <div class="addChannel">
                        <a href="" data-toggle="modal" data-target="#exampleModal">Добавить специалиста</a>
                    </div>
                </div>
            </nav>

        </div>
    </div>

</footer>

<?php wp_footer(); ?>
</body>
</html>