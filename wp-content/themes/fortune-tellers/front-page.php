<?php get_header(); ?>
<main>
    <section class="headSecondBlock">
    </section>
    <section class="blockTextButtom">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    Сео текст
                </div>
                <div class="line">
                </div>
            </div>
            <?php  if (have_posts() ) : while (have_posts() ) : the_post();?>
            <?php the_content();?>
            <?php endwhile; else : ?>
            <?php endif; ?>
        </div>
    </section>

    <section class="sliderr">
        <div class="container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php
                        $args = array(
                            'post_type' => 'slider_images',
                            'post_status' => 'publish'
                        );

                        $query = new WP_Query( $args );
                    ?>
                    <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="ind active"></li>
                    <?php endwhile; else : ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </ol>
                <div class="carousel-inner">
                    <?php
                        $args = array(
                            'post_type' => 'slider_images',
                            'post_status' => 'publish'
                        );

                        $query = new WP_Query( $args );
                    ?>
                    <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
                    <div class="carousel-item <?php echo $query->current_post >= 1 ? '' : 'active'; ?>">
                        <div class="absolutBlock" style="background: url('<?php echo $backgroundImg[0]; ?>')no-repeat; background-size:cover;">
                            <div class="title">
                                <?php the_title();?>
                            </div>
                            <div class="discrition">
                                <?php the_content();?>
                            </div>
                            <button onclick="return location.href = '<?php the_field('slide-link');?>'">Смотреть</button>
                        </div>
                    </div>
                    <?php endwhile; else : ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon iconPrev" aria-hidden="true"></span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon iconNext" aria-hidden="true"></span>
                </a>
            </div>
        </div>
    </section>

    <section class="blockForm">
        <?php get_search_form();?>
    </section>

    <section class="numberBlock">
        <div class="container">
            <div class="row centerBlock">
                <?php
                    $args = array(
                        'post_type' => 'statistic',
                        'post_status' => 'publish'
                    );

                    $query = new WP_Query( $args );
                ?>
                <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                <div class="col-lg-4 col-md-6">
                    <div class="block">
                        <div class="number">
                            <?php the_title();?>
                        </div>
                        <div class="text">
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
                <?php endwhile; else : ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </section>

    <section class="bestMagicians">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    лучшие маги
                </div>
                <div class="line">
                </div>
            </div>
            <div class="listMagic">
                <div class="row">
                    <?php
                        $top = 1;

                        $args = array(
                            'post_type' => 'fortune_tellers',
                            'post_status' => 'publish',
                            'posts_per_page' => '12',
                            'meta_key' => 'likes_data',
                            'orderby' => 'meta_value_num'
                        );

                        $query = new WP_Query( $args );
                    ?>
                    <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                    <?php 
                        $post_ID = $post->ID;
                        $metablog = new stdClass;
                        foreach( get_post_meta( $post_ID ) as $k => $v )
                        $metablog->$k = $v[0];
                    ?>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                        <div class="magic">
                            <?php the_post_thumbnail();?>
                            <div class="absolutTop">
                                Топ <?php echo  $top++;?>
                            </div>
                            <div class="boxInfo">
                                <div class="type">
                                    <?php 
                                    $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
                                    if( is_array( $cur_terms ) ){
                                        foreach( $cur_terms as $cur_term ){
                                            echo  $cur_term->name;
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="name">
                                    <?php the_title();?>
                                </div>
                                <div class="ratingBlock">
                                    <?php do_action('count_likes_html', $post_ID) ?>
                                    <div class="comit">
                                        <a href="<?php the_permalink();?>">
                                            <?php echo get_comments_number($post->ID);?>
                                        </a>
                                    </div>
                                </div>
                                <div class="discrition js-discrition">
                                    <?php echo get_the_excerpt(); ?>
                                </div>
                                <div class="skill">
                                    <?php 
                                    $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_tags' );
                                    if( is_array( $cur_terms ) ){
                                        foreach( $cur_terms as $cur_term ){
                                            echo  $cur_term->name .', ';
                                        }
                                    }
                                    ?>
                                </div>
                                <button onclick="return location.href = '<?php the_permalink();?>'">Подробнее</button>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; else : ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
                <div style="text-align: center;">
                    <button class="moreBtn" onclick="return location.href = '/rejting-magov'">Показать больше »</button>
                </div>

            </div>

        </div>
    </section>

    <section class="newArticle">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    Новые статьи
                </div>
                <div class="line">
                </div>
                <button class="moreArticle" onclick="return location.href = '/stati'">Показать все</button>
            </div>
             <div class="grid">
                <?php
                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page' => '4',
                        'order' => 'DESC'
                    );

                    $query = new WP_Query( $args );
                ?>
                <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                <div class="item">
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail();?>
                            <div class="textBottom">
                                <div class="titleInfo">
                                    <?php the_title();?>
                                </div>
                                <time><?php the_date('d F Y');?></time>
                            </div>
                        </a>
                </div>
                <?php endwhile; else : ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </section>

    <section class="newArticle">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    Последние отзывы
                </div>
                <div class="line">
                </div>
            </div>
            <div class="row">
                <?php 
                $i = 1;
                foreach (get_comments() as $comment): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="news">
                        <div class="iconAb"></div>
                        <div class="name">
                            <?php echo $comment->comment_author; ?>
                        </div>
                        <div class="discrition">
                            <?php echo $comment->comment_content; ?>
                        </div>
                        <time>
                            <?php comment_date('d.m.Y в H:i'); ?>
                        </time>
                    </div>
                </div>
                <?php $i++; ?>
                <?php if($i == 4) break; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section class="AddSpecial">
        <div class="container">
            <div class="textBlock">
                <div class="title">
                    Добавьте специалиста на сайт
                </div>
                <div class="discrip">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas
                    accumsan lacus
                </div>
                <button class="moreArticle" data-toggle="modal" data-target="#exampleModal">Добавить »</button>
            </div>
        </div>
    </section>

</main>
<?php get_footer();?>

