<?php get_header();?>
<main class="newsPage">
    <section class="headSecondBlock">
    </section>
    <div class="container zIndex">
        <ul class="breadcrumbs-one">
            <li><a href="/" rel="nofollow">Главная</a></li>
            <li><a href="/stati" rel="nofollow">Статьи</a></li>
            <li><?php the_title();?></li>
        </ul>
    </div>
    <section class="newArticle">
        <div class="container">
            <div class="mainWrapp">
                <div class="left">
                    <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail();?>
                        <div class="textBottom">
                            <div class="titleInfo">
                                <?php the_title();?>
                            </div>
                            <time><?php echo get_the_date('d F Y'); ?></time>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="contentMain">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="topMag">
                        <div class="head">
                            топ магов
                        </div>
                        <ul>
                            <?php
                                $top = 1;

                                $args = array(
                                    'post_type' => 'fortune_tellers',
                                    'post_status' => 'publish',
                                    'posts_per_page' => '5',
                                    'meta_key' => 'likes_data',
                                    'orderby' => 'meta_value_num'
                                );

                                $query = new WP_Query( $args );
                            ?>
                            <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                            <?php 
                                $post_ID = $post->ID;
                                $metablog = new stdClass;
                                foreach( get_post_meta( $post_ID ) as $k => $v )
                                $metablog->$k = $v[0];
                            ?>
                            <li>
                                <div class="name">
                                    <span><?php echo  $top++;?></span>
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </div>
                                <div class="static">
                                    <?php do_action('count_likes_html', $post_ID) ?>
                                    <div class="comit">
                                        <a href="<?php the_permalink();?>">
                                            <?php echo get_comments_number($post->ID);?>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <?php endwhile; else : ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                    <div class="newReviews hiddeMob">
                        <div class="head">
                            Новые отзывы
                        </div>
                        <?php 
                        $i = 0;
                        foreach (get_comments() as $comment): ?>
                        <a href="<?php the_permalink();?>">
                            <div class="news">
                                <div class="iconAb"></div>
                                <div class="name">
                                    <?php echo $comment->comment_author; ?>
                                </div>
                                <div class="discrition">
                                    <?php echo $comment->comment_content; ?>
                                </div>
                                <time>
                                    <?php comment_date('d.m.Y в H:i'); ?>
                                </time>
                            </div>
                        </a>
                        <?php $i++; ?>
                        <?php if($i == 1) break; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="newArticleSidbar hiddeMob">
                        <div class="head">
                            новые статьи
                        </div>
                        <ul>
                            <?php
			                    $args = array(
			                        'post_type' => 'post',
			                        'post_status' => 'publish',
			                        'posts_per_page' => '4',
			                        'order' => 'DESC'
			                    );

			                    $query = new WP_Query( $args );
			                ?>
			                <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                            <li>
                                <div class="title">
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </div>
                                <div class="discript js-discrition">
                                    <?php echo get_the_excerpt(); ?>
                                </div>
                                <a href="<?php the_permalink();?>" class="btnClick">
                                    Читать
                                </a>
                            </li>
                            <?php endwhile; else : ?>
               				<?php endif; ?>
                			<?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8">
                    <div class="textBox">
                        <?php echo the_field('content-list');?>
                        <div class="text">
                        	<?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="specialist">
        <div class="container">
            <div class="listMagic">
                <div class="row">
                	<?php
                        $top = 1;

                        $args = array(
                            'post_type' => 'fortune_tellers',
                            'post_status' => 'publish',
                            'posts_per_page' => '4',
                            'meta_key' => 'likes_data',
                            'orderby' => 'meta_value_num'
                        );

                        $query = new WP_Query( $args );
                    ?>
                    <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                    <?php 
                        $post_ID = $post->ID;
                        $metablog = new stdClass;
                        foreach( get_post_meta( $post_ID ) as $k => $v )
                        $metablog->$k = $v[0];
                    ?>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                        <div class="magic">
                            <?php the_post_thumbnail();?>
                            <div class="absolutTop">
                                <?php echo  $top++;?>
                            </div>
                            <div class="boxInfo">
                                <div class="type">
                                    <?php 
                                    $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
                                    if( is_array( $cur_terms ) ){
                                        foreach( $cur_terms as $cur_term ){
                                            echo  $cur_term->name;
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="name">
                                    <?php the_title();?>
                                </div>
                                <div class="ratingBlock">
                                    <?php do_action('count_likes_html', $post_ID) ?>
                                    <div class="comit">
                                        <a href="<?php the_permalink();?>">
                                            <?php echo get_comments_number($post->ID);?>
                                        </a>
                                    </div>
                                </div>
                                <button onclick="return location.href = '<?php the_permalink();?>'">Подробнее</button>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; else : ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer();?>