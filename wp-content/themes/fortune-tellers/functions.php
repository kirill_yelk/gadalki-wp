<?php 
/*---------Styles&Scripts---------*/
function styles(){
    wp_register_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    wp_enqueue_style( 'bootstrap' );
    wp_register_style( 'slick-styles', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
    wp_enqueue_style( 'slick-styles' );
    wp_register_style( 'font', 'https://fonts.googleapis.com/css?family=Roboto:400,400i,700');
    wp_enqueue_style( 'font' );
    wp_register_style( 'application-styles', get_template_directory_uri() . '/css/application.css', filemtime(get_template_directory() . '/css/application.css'));
    wp_enqueue_style( 'application-styles' );
    wp_register_style( 'custom-styles', get_template_directory_uri() . '/style.css', filemtime(get_template_directory() . '/style.css'));
    wp_enqueue_style( 'custom-styles' );
}
add_action( 'wp_enqueue_scripts', 'styles' );

function scripts(){
    wp_register_script( 'vendors-js', get_template_directory_uri() . '/js/vendors.min.js', array(), '', filemtime(get_template_directory() . '/js/vendors.min.js'), true );
    wp_enqueue_script( 'vendors-js' );
    wp_register_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', array(), '', filemtime(get_template_directory() . '/js/custom.js'), true );
    wp_enqueue_script( 'custom-js' );
    wp_register_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array(), '', true  );
    wp_enqueue_script( 'bootstrap-js' );
    wp_register_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '', true  );
    wp_enqueue_script( 'slick-js' );
    if(is_singular(['fortune_tellers']) ){
        wp_register_script( 'prognoz-common', get_template_directory_uri() . '/js/common.js', array(), '', filemtime(get_template_directory() . '/js/common.js'), true );
        wp_enqueue_script( 'prognoz-common' );
    } 
}
add_action( 'wp_enqueue_scripts', 'scripts' );

/*---------Ajax content include---------*/
require_once("ajax.php");

/*--------theme functions include-------*/
require_once("inc/theme-functions.php");

/*---------Enabling menu&thumbs---------*/
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

function new_submenu_class($menu) {    
    $menu = preg_replace('/ class="sub-menu"/','/ class="sub-menu boxList boxListJ" /',$menu);        
    return $menu;      
}
add_filter('wp_nav_menu','new_submenu_class'); 

//Disable gutenberg style in Front
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

remove_action( 'wp_head', 'wp_generator');
function wp_remove_version() {
    return '';
}
add_filter('the_generator', 'wp_remove_version');

function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rsd_link');

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );

remove_action( 'wp_head', 'wp_resource_hints', 2);
remove_action( 'wp_head', 'print_emoji_detection_script', 7);
remove_action( 'wp_print_styles', 'print_emoji_styles');

remove_action( 'wp_head', 'wp_shortlink_wp_head');

// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);


/*
 *
 * Remove Embeded Js 
 *
 */
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );


/**
 * Robots.
 */
add_filter('robots_txt', 'add_robotstxt');

function add_robotstxt($output){

    $output .= "Disallow: /wp-login.php\n";
    $output .= "Allow: /wp-content/uploads/\n";
    $output .= "Allow: /wp-content/themes/yelk-6/assets/\n"; 

    $output .= "Host: kaperne.com\n";
    $output .= "Sitemap: http://kaperne.com/sitemap_index.xml\n";

    return $output;
}


