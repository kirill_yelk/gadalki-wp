(function () {
    jQuery(function ($) {
        function heightWindowsPage() {
            var hh = $('header').height();
            var fh = $('footer').height();
            var wh = $(window).height();
            var сh = wh - hh - fh;
            $('main').css('min-height', сh);
        }
        function AnswerCaper() {
            $('.js-OpenAnswer').click(function (e) {
                e.preventDefault();
                $('.readAnswer').text('');
                $(this).parents('.commit').eq(0).find('.readAnswer').eq(0).append('<form id="formAnswer" class="formCommit" action="">'+
                    '<textarea cols="45" rows="12" name="comment" placeholder="Ваш комментарий" class="infoTextarea"></textarea>'+
                    '<input type="hidden" name="comment_post_ID" value="'+$('#comment_post_ID').val()+'">'+
                    '<input type="hidden" name="comment_parent" value="'+$(this).attr('data-id')+'"/>'+
                    '<button class="js-commitCaper">Отправить</button>'+
                    '</form>');

                sendAnswer();
            });
        }

        function sendAnswer() {
            $('.js-commitCaper').click(function (e) {
                e.preventDefault();
                var infoTextarea =  $(".infoTextarea").val();
                if(infoTextarea.length < 10) return alert('Введите текст больше 10 символов');

                var commitBlock = $(this).parents('.commit').eq(0);

                $.ajax({
                    type : 'POST',
                    url : 'http://' + location.host + '/wp-admin/admin-ajax.php',
                    data: $('#formAnswer').serialize() + '&action=ajaxcomments',
                    beforeSend: function(xhr){
                        // действие при отправке формы, сразу после нажатия на кнопку #submit
                        $('.js-commitCaper').text('Отправка...');
                    },
                    error: function (request, status, error) {
                        if(status==500){
                            alert('Ошибка при добавлении комментария');
                        } else if(status=='timeout'){
                            alert('Ошибка: Сервер не отвечает, попробуй ещё.');
                        } else {
                            // ворпдрессовские ошибочки, не уверен, что это самый оптимальный вариант
                            // если знаете способ получше - поделитесь
                            var errormsg = request.responseText;
                            var string1 = errormsg.split("<p>");
                            var string2 = string1[1].split("</p>");
                            alert(string2[0]);
                        }
                    },
                    success: function (newComment) {

                        // Если уже есть какие-то комментарии
                        $('#formAnswer').remove();
                        commitBlock.append(newComment);
                        // очищаем поле textarea
                        $('#addcomment').find('textarea').val('');
                    }
                });
            });
        }



        $('#addcomment').submit(function(e){

            e.preventDefault();
            var infoTextarea =  $(this).find('textarea').val();
            if(infoTextarea.length < 10) return alert('Введите текст больше 10 символов');

            $.ajax({
                type : 'POST',
                url : 'http://' + location.host + '/wp-admin/admin-ajax.php',
                data: $(this).serialize() + '&action=ajaxcomments',
                beforeSend: function(xhr){
                    // действие при отправке формы, сразу после нажатия на кнопку #submit
                    $('#addcomment').find('button').text('Отправка...');
                },
                error: function (request, status, error) {
                    if(status==500){
                        alert('Ошибка при добавлении комментария');
                    } else if(status=='timeout'){
                        alert('Ошибка: Сервер не отвечает, попробуй ещё.');
                    } else {
                        // ворпдрессовские ошибочки, не уверен, что это самый оптимальный вариант
                        // если знаете способ получше - поделитесь
                        var errormsg = request.responseText;
                        var string1 = errormsg.split("<p>");
                        var string2 = string1[1].split("</p>");
                        alert(string2[0]);
                    }
                },
                success: function (newComment) {

                    // Если уже есть какие-то комментарии
                    $('.listCommit .boxCommit').prepend(newComment);
                    // очищаем поле textarea
                    $('#addcomment').find('textarea').val('');

                    $('#addcomment').find('button').text('Отправить');
                    
                }
            });
        });

        function clickNav() {
            var widd = document.body.offsetWidth;
            if (widd <= 992) {
                $('.js-mainList').click(function () {
                    $(this).find('.boxList').toggleClass("block");
                });
            } else {
                $('.js-mainList').removeClass("block");
            }
        }

        function widthNav() {
            var bodyWidt = document.body.offsetWidth;
            document.querySelector('.boxList').style.width = bodyWidt + "px";
            var container = document.querySelector('.container').offsetLeft;
            $('.js-mainList').each(function () {
                $(this).find('.boxListJ').css("width", bodyWidt +'px');
                $(this).find('.boxListJ').css("padding-left", 45 + container +'px');
                $(this).find('.boxListJ').css("padding-right", 45 + container +'px');
            });
            var navMobil = document.querySelector(".navMobil");
            var navMobil2 = document.querySelector(".navMobil2");
            var tablet = window.matchMedia("only screen and (min-width: 992px)");
            if (!tablet.matches) {
                navMobil.style.width = bodyWidt + "px";
                navMobil.style.paddingLeft = container + "px";
                navMobil.style.paddingRight = container + "px";
                navMobil2.style.width = bodyWidt + "px";
                navMobil2.style.paddingLeft = container + "px";
                navMobil2.style.paddingRight = container + "px";
            } else {
                navMobil.style.paddingLeft = "0px";
                navMobil.style.paddingRight = "0px";
                navMobil2.style.paddingLeft = "0px";
                navMobil2.style.paddingRight = "0px";
            }
        }


        function dataAttrebutRating() {
            $('.js-rating').click(function (e) {
                e.preventDefault();
                var rating = $(this).attr("data-rating");
                $('.js-rating').css( 'pointer-events', 'none' );
                console.log(rating);
            });
        }

        function multiline() {

            $('.listMagic .js-discrition').each(function () {
                var size = 135,
                    newsContent= $(this),
                    newsText = newsContent.text();
                if(newsText.length > size){
                    newsContent.text(newsText.slice(0, size) + ' ...');
                }
            });


        }

        function chebox() {
            $('.typeMag .wrapLabel').each(function (index) {
                if(index > 5){
                    $(this).css("display", 'none');
                }
            });

            $('.uslugi .wrapLabel').each(function (index) {
                if(index > 5){
                    $(this).css("display", 'none');
                }
            });
                $.fn.clickToggle = function(func1, func2) {
                    var funcs = [func1, func2];
                    this.data('toggleclicked', 0);
                    this.click(function() {
                        var data = $(this).data();
                        var tc = data.toggleclicked;
                        $.proxy(funcs[tc], this)();
                        data.toggleclicked = (tc + 1) % 2;
                    });
                    return this;
                };

            $('.js-moreTypeMag').clickToggle(function() {
                $('.typeMag .wrapLabel').each(function () {
                    $(this).css("display", 'block');
                });
                $(this).text("Скрыть");
            }, function() {
                $('.typeMag .wrapLabel').each(function (index) {
                    if(index > 5){
                        $(this).css("display", 'none');
                    }
                });
                $(this).text("Показать все");
            });

            $('.js-moreUslugi').clickToggle(function() {
                $('.uslugi .wrapLabel').each(function () {
                    $(this).css("display", 'block');
                });
                $(this).text("Скрыть");
            }, function() {
                $('.uslugi .wrapLabel').each(function (index) {
                    if(index > 5){
                        $(this).css("display", 'none');
                    }
                });
                $(this).text("Показать все");
            });
        }

        $(document).ready(function () {
            heightWindowsPage();
            widthNav();
            AnswerCaper();
            sendAnswer();
            clickNav();
            dataAttrebutRating();
            multiline();
            chebox();
            $('.carousel').carousel({
                interval: 3500
            });

            $(".vertical-center-4").slick({
                dots: false,

                arrows: false,
                responsive: [
                    {
                        breakpoint: 99999,
                        settings: "unslick"
                    },
                    {
                        breakpoint: 769,
                        setting:{
                            speed: 300,
                            slidesToShow: 3,
                            slidesToScroll: 1

                        }
                    }
                ]
            });
        });


        $(window).resize(function () {
            heightWindowsPage();
            widthNav();
            AnswerCaper();
            sendAnswer();
            clickNav();
            multiline();
        });
    });
})();












