$( document ).ready(function(){
    /**
     * ==================== waitUntilExists ======================
     * 2/11/2016
     */
    $.fn.waitUntilExists = function(handler, shouldRunHandlerOnce, isChild) {
      var found = 'found';
      var $this = $(this.selector);
      var $elements = $this.not(function() {
        return $(this).data(found);
      }).each(handler).data(found, true);

      if (!isChild) {
        (window.waitUntilExists_Intervals = window.waitUntilExists_Intervals || {})[this.selector] =
          window.setInterval(function() {
            $this.waitUntilExists(handler, shouldRunHandlerOnce, true);
          }, 500)
        ;
      }
      else
        if (shouldRunHandlerOnce && $elements.length) {
          window.clearInterval(window.waitUntilExists_Intervals[this.selector]);
        }

      return $this;
    };
    
    (function() {

        var btn = $('div.cir');

        //btn.waitUntilExists(function() {

          btn.on('click', '.js-rating', function(e) {
            var that = $(this),
                visGol = $('.visGol').find('span'),
                ratingData = that.attr('data-rating'),
                postId = that.attr('data-id'),
                likesNumber = parseInt(visGol.text());

            $.ajax({
              url       : '/wp-admin/admin-ajax.php',
              type      : "POST",
              data      : {
                action : 'ajx20161216111200',
                post_id: postId,
                rating: ratingData
              },
              //dataType : "html",
              beforeSend: function() {
              },
              success: function(data) {
                
                that.parent('.voitBlock .cir').html('<p>Вы уже проголосавли!</p>');
                if (data === 'like') {
                  likesNumber += 1;
                  visGol.text('+'+likesNumber);
                } else {
                  likesNumber -= 1;
                  visGol.text(likesNumber);
                }
              },
              error     : function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
              }
            });
          });

        //});

    })();

});