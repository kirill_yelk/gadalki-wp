<div class="container">
    <form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="imput">
            <i class="fa fa-search"></i>
            <input type="text" name="s" id="s" placeholder="<?php esc_attr_e( 'Введите имя специалиста', 'fortune-tellers' ); ?>"/>
        </div>
        <input type="submit" class="submit findBtn" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Найти', 'fortune-tellers' ); ?>" />
    </form>
</div>

