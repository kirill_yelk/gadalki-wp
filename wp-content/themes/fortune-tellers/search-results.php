<?php
/*
	Template Name: Search Results
*/ 
get_header();
?>

<section class="contentMain">
    <div class="container">
        <div class="row">
			<div class="col-lg-3 col-md-4">
				<div class="filter">
                    <div class="head">
                        Фильтр
                    </div>
                    <div class="listFilterBlock">
                        <form method="POST" action="/results/">
							<div class="typeMag">
								<div class="nameType">
                                    Тип магов
                                </div>
								<?php 
								
								$terms = get_terms('fortune_tellers_categories');
								
								if(!empty($terms) && !is_wp_error($terms)):
									foreach($terms as $term):
										echo '<label class="wrapLabel">' . $term->name;
										echo '<input type="checkbox" name="fortune-cat[]" id="'. $term->term_id .'" value="'. $term->slug .'">';
										echo '<span class="checkmark" for="'. $term->term_id .'"></span>';
										echo '</label>';
									endforeach;
								endif;?>
								<div  class="moreCheck js-moreTypeMag">
                                    Показать все
                                </div>
							</div>
							<div class="uslugi">
								<div class="nameType">
                                    Услуги
                                </div>
								<?php
								$terms = get_terms('fortune_tellers_tags');
								
								if(!empty($terms) && !is_wp_error($terms)):
									foreach($terms as $term):
										echo '<label class="wrapLabel">' . $term->name;
										echo '<input type="checkbox" name="fortune-tag" id="'. $term->term_id .'" value="'. $term->slug .'">';
										echo '<span class="checkmark" for="'. $term->term_id .'"></span>';
										echo '</label>';
									endforeach;
								endif;
								?>
								<div  class="moreCheck js-moreTypeMag">
                                    Показать все
                                </div>
								<input type="hidden" name="submitted" value="Y">
								<input type="submit" class="btnSubmit" value="Применить"> 
							</div>
                        </form>
                    </div>
                </div>
				<div class="topMag">
                    <div class="head">
                        топ магов
                    </div>
                    <ul>
                        <?php
                            $top = 1;

                            $args = array(
                                'post_type' => 'fortune_tellers',
                                'post_status' => 'publish',
                                'posts_per_page' => '5',
                                'meta_key' => 'likes_data',
                                'orderby' => 'meta_value_num'
                            );

                            $query = new WP_Query( $args );
                        ?>
                        <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                        <?php 
                            $post_ID = $post->ID;
                            $metablog = new stdClass;
                            foreach( get_post_meta( $post_ID ) as $k => $v )
                            $metablog->$k = $v[0];
                        ?>
                        <li>
                            <div class="name">
                                <span><?php echo $top++;?></span>
                                <a href="<?php the_permalink();?>">
                                    <?php the_title();?>
                                </a>
                            </div>
                            <div class="static">
                                <?php do_action('count_likes_html', $post_ID) ?>
                                <div class="comit">
                                    <a href="<?php the_permalink();?>">
                                        <?php echo get_comments_number($post->ID);?>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <?php endwhile; else : ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </ul>
                </div>
				<div class="newReviews hiddeMob">
                    <div class="head">
                        Новые отзывы
                    </div>
                    <?php 
                    $i = 0;
                    foreach (get_comments() as $comment): ?>
                    <a href="<?php the_permalink();?>">
                        <div class="news">
                            <div class="iconAb"></div>
                            <div class="name">
                                <?php echo $comment->comment_author; ?>
                            </div>
                            <div class="discrition">
                                <?php echo $comment->comment_content; ?>
                            </div>
                            <time>
                                <?php comment_date('d.m.Y в H:i'); ?>
                            </time>
                        </div>
                    </a>
                    <?php $i++; ?>
                    <?php if($i == 1) break; ?>
                    <?php endforeach; ?>
                </div>
			</div>
			<div class="col-lg-9 col-md-8">
			<?php
				if(!empty($_POST['submitted']) == 'Y'): 
			 
					$fortuneCatArr = serialize($_POST['fortune-cat']);
					$fortuneCat = unserialize($fortuneCatArr);
					
					$fortuneTagArr = serialize($_POST['fortune-tag']);
					$fortuneTag = unserialize($fortuneTagArr);

				endif;
				$top = 1;								
				$args = array(
					"posts_per_page" => -1,
					"post_type" => "fortune_tellers",
                    'meta_key' => 'likes_data',
                    'orderby' => 'meta_value_num',
					"tax_query" => array(
			 
						"relation" => "OR",
			 
						 array(
			 
							 "taxonomy" => "fortune_tellers_categories",
							 "field" => "slug",
							 "terms" => $fortuneCat
			 
						 ),
			 
						 array(
			 
							 "taxonomy" => "fortune_tellers_tags",
							 "field" => "slug",
							 "terms" => $fortuneTag,
							 "operator" => "AND"
			 
						 ),
			 
				 ),                          
				);
				$query = new WP_Query( $args );
			?>
				<div class="textBox">
					<div class="listMagic">
						<div class="row">
						<?php while ( $query->have_posts() ) : $query->the_post();?>
						<div class="col-lg-4 col-md-6 col-6">
                            <div class="magic">
                                <?php the_post_thumbnail();?>
                                <div class="absolutTop">
                                    Топ <?php echo $top++;?>
                                </div>
                                <div class="boxInfo">
                                    <div class="type">
                                        <?php 
                                        $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_categories' );
                                        if( is_array( $cur_terms ) ){
                                            foreach( $cur_terms as $cur_term ){
                                                echo  $cur_term->name;
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="name">
                                        <?php the_title();?>
                                    </div>
                                    <div class="ratingBlock">
										<?php 
											$post_ID = $post->ID;
											$metablog = new stdClass;
											foreach( get_post_meta( $post_ID ) as $k => $v )
											$metablog->$k = $v[0];
										?>
                                        <?php do_action('count_likes_html', $post_ID) ?>
                                        <div class="comit">
                                            <a href="<?php the_permalink();?>">
                                                <?php echo get_comments_number($post->ID);?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="discrition js-discrition">
                                        <?php echo get_the_excerpt(); ?>
                                    </div>
                                    <div class="skill">
                                        <?php 
                                        $cur_terms = get_the_terms( $post->ID, 'fortune_tellers_tags' );
                                        if( is_array( $cur_terms ) ){
                                            foreach( $cur_terms as $cur_term ){
                                                echo  $cur_term->name .', ';
                                            }
                                        }
                                        ?>
                                    </div>
                                    <button onclick="return location.href = '<?php the_permalink();?>'">Подробнее</button>
                                </div>
                            </div>
                        </div>
						<?php endwhile; wp_reset_postdata();?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>