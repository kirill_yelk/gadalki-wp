<?php
// ================== Likes process Start =========================

// ================== Set Idenity for non logget ==================
add_action( 'init', 'aa_func_20163016103030' );
function aa_func_20163016103030()
{
    $user_hash = null;

    if ( ! is_user_logged_in() ) {
        $user_hash = sha1( time() );
        if ( ! isset( $_COOKIE[ 'current_user_hash' ] ) ) {
            setcookie( "current_user_hash", $user_hash, strtotime( '+30 days' ) );
        }
    }
}
// ================== Get Current User Idenity ==================
function aa_get_current_user_hash()
{
    $user_hash = null;
    if ( is_user_logged_in() ) {
        $user_hash = sha1( wp_get_current_user()->data->user_email );
    } else {
        if ( ! isset( $_COOKIE[ 'current_user_hash' ] ) ) {
            $user_hash = sha1( time() );
        } else {
            $user_hash = $_COOKIE[ 'current_user_hash' ];
        }

    }

    return $user_hash;
}
// ================== Get post Likes ==================
function post_like( $id )
{
    $meta = get_post_meta( $id, 'likes_container', true );
    $array_meta = unserialize( $meta );

    if ( empty( $array_meta ) || ( count( $array_meta ) === 0 ) ) {
        $meta = 0;
    } else {
        $meta = count( unserialize( $meta ) );
    }

    return $meta;
}

// ================== User liked this post? ==================
function user_liked_this( $post_id )
{
    $user_hash = aa_get_current_user_hash();
    $likes_arr = unserialize( get_post_meta( $post_id, 'likes_container', true ) );
    if ( is_array( $likes_arr ) ) {
        $position = array_search( $user_hash, $likes_arr );

        return $position !== false;
    }

    return false;
}

// ================== Get post Likes html ==================
add_action( 'count_likes_html', 'aa_func_201902251306', 10, 1 );
function aa_func_201902251306( $id )
{
    $likes_data = get_post_meta( $id, 'likes_data', true );
    $likes_data = $likes_data ? $likes_data : "+0";
    $likes_data = ($likes_data > 0) ? '+'.$likes_data : $likes_data; ?>

    <div class="visGol rating">Рейтинг: <b><span><?php echo $likes_data;?></span></div></b>

<?php
}

// ================== Likes html ==================
add_action( 'likes_html', 'aa_func_20160017090021', 10, 1 );
function aa_func_20160017090021( $post_id )
{
    if(user_liked_this( $post_id ) ){
        
        echo '<div class="vote"><p>Вы уже проголосавли!</p></div>';

    } else { ?>

        <div class="minus js-rating" data-rating="-1" data-id="<?= $post_id ?>">-</div>
        <div class="plus js-rating" data-rating="1" data-id="<?= $post_id ?>">+</div>

    <?php }
}
// ================== Likes process End =========================

// ================== Comments ==================
function mytheme_comment( $comment, $args, $depth ) {

    $GLOBALS['comment'] = $comment; ?>

    <?php if(empty($comment->comment_parent) ) : ?>

        <div id="commit-<?= $comment->comment_ID ?>" class="commit">
            <div class="wrappCommit">
                <?php 
                    $rate = get_comment_meta( $comment->comment_ID, 'radio-group', true);
                    if($rate == 1){
                        echo '<div class="classPlus">+</div>';
                    }else{
                        echo '<div class="classMinus">-</div>';
                    }
                ?>
                <?php echo get_avatar($comment, $size='60' , '', '', array('') ); ?>
                <div class="commitInfo">
                    <div class="commitInfo-head">
                        <div class="left">
                            <p class="name"><?php echo get_comment_author(); ?></p>
                            <time><?php echo get_comment_date('d.m.Y') ?></time>
                        </div>
                    </div>

    <?php else : ?>
       
        <div id="commit-<?= $comment->comment_ID ?>" class="commit blockAnswerCommit">
            <div class="wrappCommit">
                <?php echo get_avatar($comment, $size='60' , '', '', array('') ); ?>
                <div class="commitInfo">
                    <div class="commitInfo-head">
                        <div class="left">
                            <p class="name"><?php echo get_comment_author(); ?>
                                <span>Ответ для <b><?php echo get_comment_author($comment->comment_parent); ?></b></span></p>
                            <time><?php echo get_comment_date('d.m.Y') ?></time>
                        </div>
                    </div>

    <?php endif; ?>

                    <div class="commitText">
                        <p><?php echo get_comment_text() ?></p>
                    </div>
                    <div class="ridth">
                        <?php if(get_current_user_id() != $comment->user_id) : ?>

                            <button  class="js-OpenAnswer" data-id="<?= $comment->comment_ID ?>">Ответить</button>

                        <?php endif; ?>
                    </div>

                </div>
            </div>
            <div class="readAnswer"></div>
        
<?php }