<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title>Гадалки</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes"/>
    <title></title>

    <?php wp_head(); ?>
</head>
<body>
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a title="logo" href="/" class="styleLogo">
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo">
                <div class="logoText">
                    <p class="logoText-P">ExtraTop</p>
                    <span>Честный рейтинг магов</span>
                </div>
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarsExample05"
                    aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between navMobil js-WidthNav" id="navbarsExample05">
                
                    <?php wp_nav_menu( 
                        array(
                            'menu_name' => 'Primary',
                            'menu_class' => 'navbar-nav'
                    ) ); ?>

                <div class="addChannel">
                    <a href="" data-toggle="modal" data-target="#exampleModal">Добавить специалиста</a>
                </div>
            </div>
        </nav>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modalAddCaper">
                        <div class="deleteClick" data-dismiss="modal" aria-label="Close">X</div>
                        <h3>Добавить специалиста</h3>
                        <?php echo do_shortcode('[contact-form-7 id="42" title="Contact Form"]');?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</header>