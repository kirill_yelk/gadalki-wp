<?php get_header(); ?>
<main class="newsListPage">
	<section class="headSecondBlock">
    </section>
    <div class="container zIndex">
        <ul class="breadcrumbs-one">
            <li><a href="/" rel="nofollow">Главная</a></li>
            <li>Результаты поиска</li>
        </ul>
    </div>
    <section class="blockForm">
        <?php get_search_form();?>
    </section>
    <section class="listArticles">
        <div class="container">
            <div class="row">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                    <a href="<?php the_permalink();?>">
                        <div class="article">
                            <a href="<?php the_permalink();?>">
                                <?php the_post_thumbnail();?>
                            </a>
                            <div class="discrition">
                                <?php the_title();?>
                            </div>
                            <time>
                                <?php the_date('d F Y');?>
                            </time>
                        </div>
                    </a>
                </div>
                <?php endwhile;?>
			    <?php else :
			    echo 'По Вашему запросу ничего не найдено';
			    endif;?>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>