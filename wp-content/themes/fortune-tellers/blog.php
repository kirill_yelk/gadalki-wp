<?php 
/*
*
*Template Name: blog
*
*/
get_header();?>
<main class="newsListPage">
    <section class="headSecondBlock">
    </section>
    <section class="blockTextButtom">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    Сео текст
                </div>
                <div class="line">
                </div>
            </div>
            <?php  if (have_posts() ) : while (have_posts() ) : the_post();?>
            <?php the_content();?>
            <?php endwhile; else : ?>
            <?php endif; ?>
        </div>
    </section>

    <div class="container zIndex">
        <ul class="breadcrumbs-one">
            <li><a href="/" rel="nofollow">Главная</a></li>
            <li>Статьи</li>
        </ul>
    </div>

    <section class="blockForm">
        <?php get_search_form();?>
    </section>

    <section class="newArticle">
        <div class="container">
            <div class="titleBlock">
                <div class="text">
                    статьи
                </div>
                <div class="line">
                </div>
            </div>
            <div class="grid">
                <?php
                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page' => '4',
                        'order' => 'DESC'
                    );

                    $query = new WP_Query( $args );
                ?>
                <?php  if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post();?>
                <div class="item">
                        <a href="<?php the_permalink();?>">
                            <?php the_post_thumbnail();?>
                            <div class="textBottom">
                                <div class="titleInfo">
                                    <?php the_title();?>
                                </div>
                                <time><?php the_date('d F Y');?></time>
                            </div>
                        </a>
                </div>
                <?php endwhile; else : ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </section>

    <section class="listArticles">
        <div class="container">
            <div class="row">
                <?php
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page' => '16',
                        'order' => 'DESC',
                        'offset' => 4,
                        'paged' => $paged
                    );

                    $myposts = new WP_Query( $args );
                ?>
                <?php  if ($myposts -> have_posts() ) : while ($myposts -> have_posts() ) : $myposts -> the_post();?>
                <div class="col-xl-3 col-lg-4 col-md-6 col-6">
                    <a href="<?php the_permalink();?>">
                        <div class="article">
                            <a href="<?php the_permalink();?>">
                                <?php the_post_thumbnail();?>
                            </a>
                            <div class="discrition">
                                <?php the_excerpt();?>
                            </div>
                            <time>
                                <?php the_date('d F Y');?>
                            </time>
                        </div>
                    </a>
                </div>
                <?php endwhile; else : ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
                
            </div>
            <div class="paginationBlock">
                <div class="mainWrapp">
                    <div class="paginationNav">
                        <?php 
                                    
                            $big = 999999999; // уникальное число

                            echo paginate_links( array(
                                'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format'  => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total'   => $myposts->max_num_pages,
                                'prev_text'    => '«',
                                'next_text'    => '»',
                            ) ); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer();?>